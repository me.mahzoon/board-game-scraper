# -*- coding: utf-8 -*-

"""Version."""

VERSION = (2, 17, 1)
__version__ = ".".join(map(str, VERSION))
